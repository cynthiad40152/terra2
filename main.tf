resource "azurerm_resource_group" "myterraformgroup" {
    name     = "${var.rgname}"
    location = "${var.location}"
    tags {
        environment = "${var.environement}"
    }
}

resource "azurerm_virtual_network" "myterraformnetwork" {
    name                = "${var.netname}"
    address_space       = "${var.address}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    tags {
        environment = "${var.environement}"
    }
}
#VM4
resource "azurerm_virtual_network" "myterraformnetworkVM4" {
  name                = "VM4"
  address_space       = "${var.address}"
  location            = "northeurope"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  tags {
    environment = "${var.environement}"
  }
}
resource "azurerm_subnet" "myterraformsubnet" {
    name                 = "${var.subnetname}"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
    address_prefix       = "${var.adress_subnet}"
}
#VM4
resource "azurerm_subnet" "myterraformsubnetVM4" {
  name                 = "VM4"
  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
  virtual_network_name = "${azurerm_virtual_network.myterraformnetworkVM4.name}"
  address_prefix       = "${var.adress_subnet}"
}
resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "${var.publicipname}"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "${var.adress_allocation[0]}"

    tags {
        environment = "${var.environement}"
    }
}
#VM4
resource "azurerm_public_ip" "myterraformpublicipVM4" {
  name                         = "VM4"
  location                     = "northeurope"
  resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
  public_ip_address_allocation = "${var.adress_allocation[0]}"

  tags {
    environment = "${var.environement}"
  }
}

/*
resource "azurerm_public_ip" "myterraformpublicip2" {
    name                         = "${var.publicipname2}"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    public_ip_address_allocation = "${var.adress_allocation}"

    tags {
        environment = "${var.location}"
    }
}
*/
resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "${var.securityname}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    
    tags {
        environment = "${var.environement}"
    }

security_rule {
        name                       = "${var.rule[0]}"
        priority                   = "${var.rulepriority[0]}"
        direction                  = "${var.ruledirection[0]}"
        access                     = "${var.ruleaccess}"
        protocol                   = "${var.ruleprotocol}"
        source_port_range          = "${var.ruleportrange}"
        destination_port_range     = "${var.ruledestrange[0]}"
        source_address_prefix      = "${var.addressprefix}"
        destination_address_prefix = "${var.destaddressprefix}"
    }
}
#VM4
resource "azurerm_network_security_group" "myterraformnsgVM4" {
  name                = "VM4"
  location            = "northeurope"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"


  tags {
    environment = "${var.environement}"
  }

  security_rule {
    name                       = "${var.rule[0]}"
    priority                   = "${var.rulepriority[0]}"
    direction                  = "${var.ruledirection[0]}"
    access                     = "${var.ruleaccess}"
    protocol                   = "${var.ruleprotocol}"
    source_port_range          = "${var.ruleportrange}"
    destination_port_range     = "${var.ruledestrange[0]}"
    source_address_prefix      = "${var.addressprefix}"
    destination_address_prefix = "${var.destaddressprefix}"
  }
}
/*
resource "azurerm_network_security_group" "myterraformnsg2" {
    name                = "${var.securityname2}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    
    tags {
        environment = "${var.environement}"
    }

security_rule {
        name                       = "${var.rule[0]}"
        priority                   = "${var.rulepriority[0]}"
        direction                  = "${var.ruledirection [0]}"
        access                     = "${var.ruleaccess}"
        protocol                   = "${var.ruleprotocol}"
        source_port_range          = "${var.ruleportrange}"
        destination_port_range     = "${var.ruledestrange[0]}"
        source_address_prefix      = "${var.addressprefix}"
        destination_address_prefix = "${var.destaddressprefix}"
    }

security_rule {
        name                       = "${var.rule[1]}"
        priority                   = "${var.rulepriority[1]}"
        direction                  = "${var.ruledirection[1]}"
        access                     = "${var.ruleaccess}"
        protocol                   = "${var.ruleprotocol}"
        source_port_range          = "${var.ruleportrange}"
        destination_port_range     = "${var.ruledestrange[1]}"
        source_address_prefix      = "${var.addressprefix}"
        destination_address_prefix = "${var.destaddressprefix}"
    }
}
*/
resource "azurerm_network_interface" "myterraformnic" {
    #name                = "${var.interface}"
    name                = "nic${count.index}"
    count               = "${length(var.ip_addresses)-1}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"


    ip_configuration {
        name                          = "${var.ip_configuration}"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "${var.adress_allocation[1]}"
        #public_ip_address_id          = "${element(azurerm_public_ip.myterraformpublicip.*.id, count.index)}"
        #public_ip_address_id          = "${ count.index == 3 ? azurerm_public_ip.myterraformpublicip.id: "" }"
        #public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip.*.id[count.index]}"
        private_ip_address = "${element(var.ip_addresses, count.index)}"
    }

    tags {
        environment = "${var.environement}"
    }
}

resource "azurerm_network_interface" "myterraformnic2" {
  #name                = "${var.interface}"
  name                = "nic3"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"


  ip_configuration {
    name                          = "${var.ip_configuration}"
    subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "${var.adress_allocation[1]}"
    #public_ip_address_id          = "${element(azurerm_public_ip.myterraformpublicip.*.id, count.index)}"
    #public_ip_address_id          = "${ count.index == 3 ? azurerm_public_ip.myterraformpublicip.id: "" }"
    public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip.id}"
    private_ip_address = "${var.ip_addresses[3]}"
  }

  tags {
    environment = "${var.environement}"
  }
}
#VM4
resource "azurerm_network_interface" "myterraformnicVM4" {
  #name                = "${var.interface}"
  name                = "nic4"
  location            = "northeurope"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_security_group_id = "${azurerm_network_security_group.myterraformnsgVM4.id}"


  ip_configuration {
    name                          = "VM4"
    subnet_id                     = "${azurerm_subnet.myterraformsubnetVM4.id}"
    private_ip_address_allocation = "${var.adress_allocation[1]}"
    #public_ip_address_id          = "${element(azurerm_public_ip.myterraformpublicip.*.id, count.index)}"
    #public_ip_address_id          = "${ count.index == 3 ? azurerm_public_ip.myterraformpublicip.id: "" }"
    public_ip_address_id          = "${azurerm_public_ip.myterraformpublicipVM4.id}"
    private_ip_address = "${var.ip_addresses[3]}"
  }

  tags {
    environment = "${var.environement}"
  }
}
/*
resource "azurerm_network_interface" "myterraformnic3" {
  name                = "nic3"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"


  ip_configuration {
    name                          = "${var.ip_configuration}"
    subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "${var.adress_allocation[1]}"
    #public_ip_address_id          = "${element(azurerm_public_ip.myterraformpublicip.*.id, count.index)}"
    public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip.3.id[3]}"
    private_ip_address = "${element(var.ip_addresses, count.index)}"
  }

  tags {
    environment = "${var.environement}"
  }
}
*/
/*
resource "azurerm_network_interface" "myterraformnic2" {
    name                = "${var.interface2}"
    location            = "${var.location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg2.id}"


    ip_configuration {
        name                          = "${var.ip_configuration2}"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "${var.adress_allocation}"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip2.id}"
    }

    tags {
        environment = "${var.environement}"
    }
}
*/
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.myterraformgroup.name}"
    }

    byte_length = "${var.byte_l}"
}
/*
resource "random_id" "randomId2" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.myterraformgroup.name}"
    }

    byte_length = "${var.byte_l}"
}
*/
resource "azurerm_storage_account" "mystorageaccount" {
    name                = "diag${random_id.randomId.hex}${count.index}"
    count               = "${length(var.ip_addresses)}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    location            = "${var.location}"
    account_replication_type = "${var.accountreplicationtype}"
    account_tier = "${var.accountiers}"

    tags {
        environment = "${var.environement}"
    }
}
#VM4
resource "azurerm_storage_account" "mystorageaccountVM4" {
  name                = "diag${random_id.randomId.hex}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  location            = "northeurope"
  account_replication_type = "${var.accountreplicationtype}"
  account_tier = "${var.accountiers}"

  tags {
    environment = "${var.environement}"
  }
}
/*
resource "azurerm_storage_account" "mystorageaccount2" {
    name                = "diag${random_id.randomId2.hex}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    location            = "${var.location}"
    account_replication_type = "${var.accountreplicationtype}"
    account_tier = "${var.accountiers}"

    tags {
        environment = "${var.environement}"
    }
}
*/

resource "azurerm_virtual_machine" "myterraformvm" {
    #name                  = "${var.vmname}"
    name                  = "VM${count.index}"
    count                 = "${length(var.ip_addresses)-1}"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${element(azurerm_network_interface.myterraformnic.*.id,count.index)}"]
    vm_size               = "Standard_DS1_v2"

storage_os_disk {
        name              = "${var.diskname}${count.index}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }


storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "${var.computername}${count.index}"
        admin_username = "${var.user}"
    }
os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "${var.path}"
	    key_data = "${var.key}"

        }
    }

    boot_diagnostics {
        enabled     = "true"
        storage_uri = "${element(azurerm_storage_account.mystorageaccount.*.primary_blob_endpoint, count.index)}"
    }

    tags {
        environment = "${var.environement}"
    }
}


resource "azurerm_virtual_machine" "myterraformvm2" {
    name                  = "VM3"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic2.id}"]
    vm_size               = "Standard_DS1_v2"

    storage_os_disk {
        name              = "MyOsDisk3"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }
storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }

    os_profile {
        computer_name  = "${var.computername2}"
        admin_username = "${var.user2}"
	admin_password = "${var.pass}"
    }
os_profile_linux_config {
        disable_password_authentication = false
       }

    boot_diagnostics {
        enabled     = "true"
        storage_uri = "${azurerm_storage_account.mystorageaccount.3.primary_blob_endpoint}"
    }

    tags {
        environment = "${var.environement}"
    }
}
#VM4
resource "azurerm_virtual_machine" "myterraformVM4" {
  name                  = "VM4"
  location              = "northeurope"
  resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
  network_interface_ids = ["${azurerm_network_interface.myterraformnicVM4.id}"]
  vm_size               = "Standard_DS1_v2"

  storage_os_disk {
    name              = "MyOsDisk4"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }
  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "${var.computername2}"
    admin_username = "${var.user2}"
    admin_password = "${var.pass}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = "${azurerm_storage_account.mystorageaccountVM4.primary_blob_endpoint}"
  }

  tags {
    environment = "${var.environement}"
  }
}
